
module "supporting-document-bucket" {
  source           = "git::https://gitlab.com/Yang57670838/terraform-modules-template.git//bucket-for-document-storage?ref=main"
  target_region    = var.target_region
  ui_upload_domain = var.ui_upload_domain
  bucket_name      = "supporting-document-bucket"
  env = var.env
}

// this folder will gone after 1 day, since every objects inside temporary folder have 1 day age
// however it will not affect anything, we can create folder when we upload files
// but here just create the folder resource for document purpose..
resource "google_storage_bucket_object" "folder_placeholder" {
  bucket  = module.supporting-document-bucket.bucket_name // bucket name can directly know from here, but use from module output s example
  name    = "temporary/" # This creates a folder-like prefix in the bucket
  content = "test"       #  random content to act as a placeholder
}

# Create the service account to let cloud function can work on previous created bucket, 
#  and generate signed URL with this service account's permission in a limited period..
resource "google_service_account" "cloud_function_for_bucket_sa" {
  account_id   = "sa-for-cloud-function-bucket"
  display_name = "Service Account for cloud function work on bucket"
}

# Grant the Storage Object Viewer role on the bucket to the service account
resource "google_storage_bucket_iam_member" "storage_admin_member" {
  bucket = module.supporting-document-bucket.bucket_name  // bucket name can directly know from here, but use from module output s example
  role   = "roles/storage.admin" // this bucket may need to upload, change folder, delete, etc..
  member = "serviceAccount:${google_service_account.cloud_function_for_bucket_sa.email}"
}

# # Assign the Service Account Token Creator role to the service account for generating signed URLs
resource "google_project_iam_member" "service_account_token_creator" {
  project = var.project_id
  role    = "roles/iam.serviceAccountTokenCreator"
  member  = "serviceAccount:${google_service_account.cloud_function_for_bucket_sa.email}"
}


#################################### pub/sub to send messages to webhook of cloud function endpoint
# related to ask cloud function to send a mobile txt message

# schema
resource "google_pubsub_schema" "subpub_mobile_message_schema" {
  name       = "subpub_mobile_message_schema-schema_v2"
  type       = "AVRO"
  definition = file("subpub_mobile_message_schema.json")
}

resource "google_pubsub_topic" "send_engage_chat_msg_to_mobile_topic" {
  name = "send_engage_chat_msg_to_mobile_topic"
  schema_settings {
    schema   = google_pubsub_schema.subpub_mobile_message_schema.id
    encoding = "JSON" # Use "BINARY" for Protobuf schemas
  }
}

resource "google_pubsub_subscription" "webhook_for_cloud_function" {
  name  = "webhook_for_cloud_function"
  topic = google_pubsub_topic.send_engage_chat_msg_to_mobile_topic.name

  # Set the acknowledgment deadline (in seconds)
  ack_deadline_seconds = 300

  # Set the message retention duration (in seconds)
  message_retention_duration = "604800s" # 7 days in seconds

  # Push configuration
  push_config {
    push_endpoint = var.function_url_send_mobile_txt
  }
}


#################################### Cloud run, registry
resource "google_artifact_registry_repository" "engage_app_registry" {
  location      = var.target_region
  repository_id = "engage-app-registry"
  description   = "Registry for engage app images"
  format        = "DOCKER"

  vulnerability_scanning_config {
    enablement_config = "DISABLED"
  }

  cleanup_policies {
    id     = "delete_old_images"
    action = "DELETE"

    condition {
      older_than = "30d" # Delete artifacts older than 30 days
    }
  }
}


#################################### bff
# TODO: when module releases, use release tag instead of main branch
module "cloud-run-micro-service-nestjs-bff" {
  source                                     = "git::https://gitlab.com/Yang57670838/terraform-modules-template.git//cloud-run-micro-service?ref=main"
  target_region                              = var.target_region
  cloud_run_micro_service_image_registry_url = var.nestjs_bff_cloud_run_image_registry_url
  cloud_run_name                             = "nestjs-bff"
}
