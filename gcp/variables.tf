variable "function_url_send_mobile_txt" {
  default     = "https://your-push-endpoint-url"
  type        = string
  description = "the cloud function url which will send mobile txt, pub/sub will push message to here"
}

variable "target_region" {
  default     = "australia-southeast2"
  type        = string
  description = "the target region for all resources"
}

variable "project_id" {
  default = "random_project_12345"
  type    = string
}

variable "nestjs_bff_cloud_run_image_registry_url" {
  default = "random_url_12345"
  type    = string
}

variable "env" {
  default = "staging" // staging or prod
  type    = string
}

variable "ui_upload_domain" {
  default = "http://localhost:3000" // TODO: change to real UI domain
  type    = string
}
