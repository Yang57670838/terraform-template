provider "google" {
  project = "myns-382511" # TODO: use environment for production later
  region  = "australia-southeast2"
  zone    = "australia-southeast2-a"
  credentials = "keys.json"
}
